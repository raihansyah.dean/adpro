package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {
    @GetMapping("/about_me")
    public String aboutMe(@RequestParam(name = "name", required = false)
                                  String name, Model model) {
        if (name == null || name.equals("")) {
            model.addAttribute("name", "This is my CV");
        } else {
            model.addAttribute("name", name + ", I hope you're interested to hire me");
        }

        return "about_me";
    }
}
