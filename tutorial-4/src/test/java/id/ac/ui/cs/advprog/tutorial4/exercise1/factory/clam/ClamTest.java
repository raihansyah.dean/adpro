package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ClamTest {

    private BlackClams blackClams;
    private FreshClams freshClams;
    private FrozenClams frozenClams;

    @Before
    public void setUp() {
        blackClams = new BlackClams();
        freshClams = new FreshClams();
        frozenClams = new FrozenClams();
    }

    @Test
    public void testClamIngredients() {
        assertEquals(blackClams.toString(), "Black Clams from the Pacific");
        assertEquals(freshClams.toString(), "Fresh Clams from Long Island Sound");
        assertEquals(frozenClams.toString(), "Frozen Clams from Chesapeake Bay");
    }
}
