package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SauceTest {

    private BlackInkSauce blackInkSauce;
    private MarinaraSauce marinaraSauce;
    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() {
        blackInkSauce = new BlackInkSauce();
        marinaraSauce = new MarinaraSauce();
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void testSauceIngredients() {
        assertEquals(blackInkSauce.toString(), "Black Ink Sauce");
        assertEquals(marinaraSauce.toString(), "Marinara Sauce");
        assertEquals(plumTomatoSauce.toString(), "Tomato sauce with plum tomatoes");
    }
}
