package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory theFactory;

    @Before
    public void setUp() {
        theFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {
        ThinCrustDough thinCrustDough = new ThinCrustDough();
        assertEquals(theFactory.createDough().toString(), thinCrustDough.toString());
        assertEquals(theFactory.createDough().getClass(), thinCrustDough.getClass());
    }

    @Test
    public void testCreateSauce() {
        MarinaraSauce marinaraSauce = new MarinaraSauce();
        assertEquals(theFactory.createSauce().toString(), marinaraSauce.toString());
        assertEquals(theFactory.createSauce().getClass(), marinaraSauce.getClass());
    }

    @Test
    public void testCreateCheese() {
        ReggianoCheese reggianoCheese = new ReggianoCheese();
        assertEquals(theFactory.createCheese().toString(), reggianoCheese.toString());
        assertEquals(theFactory.createCheese().getClass(), reggianoCheese.getClass());
    }

    @Test
    public void testCreateVeggies() {
        Veggies[] veggies = {new Garlic(), new Onion(),
                             new Mushroom(), new RedPepper()};

        Veggies[] nyVeggies = theFactory.createVeggies();

        for (int i = 0; i < veggies.length; i++) {
            assertEquals(veggies[i].toString(), nyVeggies[i].toString());
            assertEquals(veggies[i].getClass(), nyVeggies[i].getClass());
        }
    }

    @Test
    public void testCreateClam() {
        FreshClams freshClams = new FreshClams();
        assertEquals(theFactory.createClam().toString(), freshClams.toString());
        assertEquals(theFactory.createClam().getClass(), freshClams.getClass());
    }
}
