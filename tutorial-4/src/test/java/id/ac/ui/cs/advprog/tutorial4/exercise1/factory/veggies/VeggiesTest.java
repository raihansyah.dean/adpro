package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class VeggiesTest {

    private BlackOlives blackOlives;
    private Cucumber cucumber;
    private Eggplant eggplant;
    private Garlic garlic;
    private Mushroom mushroom;
    private Onion onion;
    private RedPepper redPepper;
    private Spinach spinach;

    @Before
    public void setUp() {
        blackOlives = new BlackOlives();
        cucumber = new Cucumber();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
    }

    @Test
    public void testVeggiesIngredients() {
        assertEquals(blackOlives.toString(), "Black Olives");
        assertEquals(cucumber.toString(), "Cucumber");
        assertEquals(eggplant.toString(), "Eggplant");
        assertEquals(garlic.toString(), "Garlic");
        assertEquals(mushroom.toString(), "Mushrooms");
        assertEquals(onion.toString(), "Onion");
        assertEquals(redPepper.toString(), "Red Pepper");
        assertEquals(spinach.toString(), "Spinach");
    }
}
