package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CheeseTest {

    private BlueCheese blueCheese;
    private MozzarellaCheese mozzarellaCheese;
    private ParmesanCheese parmesanCheese;
    private ReggianoCheese reggianoCheese;

    @Before
    public void setUp() {
        blueCheese = new BlueCheese();
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
        reggianoCheese = new ReggianoCheese();
    }

    @Test
    public void testCheeseIngredients() {
        assertEquals(blueCheese.toString(), "Blue Cheese");
        assertEquals(mozzarellaCheese.toString(), "Shredded Mozzarella");
        assertEquals(parmesanCheese.toString(), "Shredded Parmesan");
        assertEquals(reggianoCheese.toString(), "Reggiano Cheese");
    }
}
