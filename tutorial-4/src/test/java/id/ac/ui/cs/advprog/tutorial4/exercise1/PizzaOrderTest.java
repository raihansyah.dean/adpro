package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class PizzaOrderTest {

    private NewYorkPizzaStore nyStore;
    private DepokPizzaStore depokStore;

    @Before
    public void setUp() throws Exception {
        nyStore = new NewYorkPizzaStore();
        depokStore = new DepokPizzaStore();
    }

    @Test
    public void testCheesePizzaOrder() {

        Pizza pizza1 = nyStore.createPizza("cheese");
        assertEquals(pizza1.getName(), "New York Style Cheese Pizza");

        Pizza pizza2 = depokStore.createPizza("cheese");
        assertEquals(pizza2.getName(), "Depok Style Cheese Pizza");
    }

    @Test
    public void testVeggiePizzaOrder() {

        Pizza pizza1 = nyStore.createPizza("veggie");
        assertEquals(pizza1.getName(), "New York Style Veggie Pizza");

        Pizza pizza2 = depokStore.createPizza("veggie");
        assertEquals(pizza2.getName(), "Depok Style Veggie Pizza");
    }

    @Test
    public void testClamPizzaOrder() {

        Pizza pizza1 = nyStore.createPizza("clam");
        assertEquals(pizza1.getName(), "New York Style Clam Pizza");

        Pizza pizza2 = depokStore.createPizza("clam");
        assertEquals(pizza2.getName(), "Depok Style Clam Pizza");
    }
}
