package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BlueCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.BlackClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MediumCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BlackInkSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Cucumber;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory theFactory;

    @Before
    public void setUp() {
        theFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {
        MediumCrustDough mediumCrustDough = new MediumCrustDough();
        assertEquals(theFactory.createDough().toString(), mediumCrustDough.toString());
        assertEquals(theFactory.createDough().getClass(), mediumCrustDough.getClass());
    }

    @Test
    public void testCreateSauce() {
        BlackInkSauce blackInkSauce = new BlackInkSauce();
        assertEquals(theFactory.createSauce().toString(), blackInkSauce.toString());
        assertEquals(theFactory.createSauce().getClass(), blackInkSauce.getClass());
    }

    @Test
    public void testCreateCheese() {
        BlueCheese blueCheese = new BlueCheese();
        assertEquals(theFactory.createCheese().toString(), blueCheese.toString());
        assertEquals(theFactory.createCheese().getClass(), blueCheese.getClass());
    }

    @Test
    public void testCreateVeggies() {
        Veggies[] veggies = {new Cucumber(), new Spinach(),
                             new Mushroom(), new BlackOlives()};

        Veggies[] depokVeggies = theFactory.createVeggies();

        for (int i = 0; i < veggies.length; i++) {
            assertEquals(veggies[i].toString(), depokVeggies[i].toString());
            assertEquals(veggies[i].getClass(), depokVeggies[i].getClass());
        }
    }

    @Test
    public void testCreateClam() {
        BlackClams blackClams = new BlackClams();
        assertEquals(theFactory.createClam().toString(), blackClams.toString());
        assertEquals(theFactory.createClam().getClass(), blackClams.getClass());
    }
}
