package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class BlackClams implements Clams {
    public String toString() {
        return "Black Clams from the Pacific";
    }
}
