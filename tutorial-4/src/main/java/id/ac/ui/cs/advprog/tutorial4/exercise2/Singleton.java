package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // Volatile -> Ensures multiple threads to handle instance correctly when initialized
    private static volatile Singleton uniqueInstance;

    private Singleton() {
    }

    // Synchronize -> Force every thread to wait its turn
    public static Singleton getInstance() {
        if (uniqueInstance == null) {
            synchronized (Singleton.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new Singleton();
                }
            }
        }
        return uniqueInstance;
    }
}
