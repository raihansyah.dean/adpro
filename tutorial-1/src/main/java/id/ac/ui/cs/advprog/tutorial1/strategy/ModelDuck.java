package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {

    public ModelDuck() {
        setFlyBehavior((FlyBehavior) new FlyNoWay());
        setQuackBehavior((QuackBehavior) new MuteQuack());
    }

    @Override
    public void display() {
        System.out.println("I'm a model duck.");
    }

}
