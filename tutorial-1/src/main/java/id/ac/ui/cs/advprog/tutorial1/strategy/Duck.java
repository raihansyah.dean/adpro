package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public abstract void display();

    public void swim() {
        System.out.println("All ducks can float of course...");
    }

    // Setter
    public void setFlyBehavior(FlyBehavior flying) {
        flyBehavior = flying;
    }

    public void setQuackBehavior(QuackBehavior quacking) {
        quackBehavior = quacking;
    }
}
