package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        this.role = "Front End Programmer";

        if (salary >= 30000) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
