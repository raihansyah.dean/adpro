package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;

public class CompositeMain {
    public static void main(String[] args) {
        Company myCompany = new Company();

        Ceo theCeo = new Ceo("Dean", 300000);
        myCompany.addEmployee(theCeo);

        Cto theCto = new Cto("James", 250000);
        myCompany.addEmployee(theCto);

        BackendProgrammer theBackend = new BackendProgrammer("Christoff", 80000);
        myCompany.addEmployee(theBackend);

        FrontendProgrammer theFrontend = new FrontendProgrammer("Sylvia", 40000);
        myCompany.addEmployee(theFrontend);

        NetworkExpert theNetworker = new NetworkExpert("Cynthia", 50000);
        myCompany.addEmployee(theNetworker);

        SecurityExpert theSecurity = new SecurityExpert("Mustabai", 70000);
        myCompany.addEmployee(theSecurity);

        UiUxDesigner theUiUx = new UiUxDesigner("Queenie", 90000);
        myCompany.addEmployee(theUiUx);

        List<Employees> employees = myCompany.getAllEmployees();
        for (Employees e : employees) {
            System.out.printf("Name    : %s\n", e.getName());
            System.out.printf("Role    : %s\n", e.getRole());
            System.out.printf("Salary  : %s\n", e.getSalary());
            System.out.println("========================");
        }
        System.out.printf("Total Monthly Salaries: %f", myCompany.getNetSalaries());
    }
}
