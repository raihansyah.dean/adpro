package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {
    public static void main(String[] args) {

        // Order number 1
        Food myBreakfast = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.printf("Your Order   : %s\n",myBreakfast.getDescription());
        System.out.printf("Total Price  : %f\n", myBreakfast.cost());

        // Add Chicken Meat
        myBreakfast = FillingDecorator.CHICKEN_MEAT.addFillingToBread(myBreakfast);
        System.out.printf("Your Order   : %s\n",myBreakfast.getDescription());
        System.out.printf("Total Price  : %f\n", myBreakfast.cost());

        // Add Chili Sauce
        myBreakfast = FillingDecorator.CHILI_SAUCE.addFillingToBread(myBreakfast);
        System.out.printf("Your Order   : %s\n",myBreakfast.getDescription());
        System.out.printf("Total Price  : %f\n", myBreakfast.cost());

        // Add Cheese
        myBreakfast = FillingDecorator.CHEESE.addFillingToBread(myBreakfast);
        System.out.printf("Your Order   : %s\n",myBreakfast.getDescription());
        System.out.printf("Total Price  : %f\n", myBreakfast.cost());

    }
}
